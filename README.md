# Linux on MacbookPro13,3
My own guide in attempting to get linux running well on my MacBooks
Any comments, suggestions, etc. are greatly apreciated.

[TOC]

## prerequisits:

**This guide of mine assumes the use of arch installed onto a usb of sorts**
**to install debian with refind on BTRFS via Debootstrap**
a link to my guide on creating such usb can be found [here](https://gitlab.com/-/snippets/3603802)

I will show you here how to get wifi running on the usb in question:
download the firmware file that can be found [here](https://bugzilla.kernel.org/show_bug.cgi?id=193121#c74)
mount root partition of usb.
mv ~/Downloads/brcmfmac43602-pcie.txt /mnt/lib/firmware/brcm
boot usb on macbook
`modprobe brcmfmac`

## Install



### make sure wiki is connected:
[here](https://wiki.archlinux.org/title/MacBookPro11,x#Internet)

### partitions ur disk & mount:
```
512M EFI partition:
    ef00 type
    mkfs.fat -F 32 /dev/sdx1
Partition for macos:
    8300 type
   mkfs.fat -F 32 /dev/sdx2
Partition to btrfs:
    8300 type
    mkfs.btrfs /dev/sdx3
```
I prefer to install macos first.
since im starting this all fresh, i partitioned in linux first,
then booted macos installer to partition /dev/sdx2, and install onto there.
```
mount /dev/sdx3 /mnt
cd /mnt
mkdir debian
btrfs subvolume create debian/@root
btrfs subvolume create debian/@home
btrfs subvolume create debian/@swap
btrfs subvolume create debian/@snapshots

cd
umount /mnt

mount /dev/sdx3 -o compress=zstd:2,subvol=debian/@root /mnt
mkdir /mnt/{home,.snapshots,.swap}
mount /dev/sdx3 -o compress=zstd:2,subvol=debian/@home /mnt/home
mount /dev/sdx3 -o compress=zstd:10,subvol=debian/@snapshots /mnt/.snapshots
mount /dev/sdx3 -o compress=none,subvol=debian/@swap /mnt/swap
mkdir -p /mnt/boot/efi
mount /dev/sdx1 /mnt/boot/efi
```
### install system:
`debootstrap --arch amd64 stable /mnt https://deb.debian.org/debian`

```
mount --make-rslave --rbind /proc /mnt/proc
mount --make-rslave --rbind /sys /mnt/sys
mount --make-rslave --rbind /dev /mnt/dev
mount --make-rslave --rbind /run /mnt/run
**might not have to do all that above, its copies from somewhere else**
LANG=C.UTF-8 chroot /mnt /bin/bash

`echo 'nameserver 8.8.4.4' | tee -a /etc/resolv.conf`

```
add the following to the single line in `/etc/apt/sources.list`:
`contrib non-free non-free-firmware`

install packages:
```
apt update
apt install linux-image-amd64 vim intel-microcode locales firmware-linux make gcc git libx11-dev libxft-dev libxinerama-dev xorg sudo iwd systemd-resolved btrfs-progs firmware-b43-installer gdisk usbutils

/usr/sbin/dpkg-reconfigure tzdata
/usr/sbin/dpkg-reconfigure locales
/usr/sbin/locale-gen
```
`btrfs filesystem mkswapfile --size 16G /.swap/swapfile`
exit chroot then run 
```
swapon /mnt/.swap/swapfile
genfstab -U /mnt >> /mnt/etc/fstab
```
edit fstab to look like the following:
```
a
```

git clone my rEFInd repo and setup accordingly

```
passwd
useradd user -m
passwd user
```
`sytemctl enable systemd-netwokd systemd-resolved iwd`

`vim /etc/systemd/network/25-wireless.network`
```
[Match]
Name=wlan0

[Network]
DHCP=yes
IgnoreCarrierLoss=3s
```

`efibootmgr -c -d /dev/sdx1 -l \\refind\\refind_x64.efi -L "rEFInd"`

## Post Install:

### Wifi

#### Prerequisit

following the link found [here](https://bugzilla.kernel.org/show_bug.cgi?id=193121),
I downloaded the files found [here](https://bugzilla.kernel.org/show_bug.cgi?id=193121#c18) and [here](https://bugzilla.kernel.org/show_bug.cgi?id=193121#c74)
I moved them both to `/lib/firmware/brcm/`. Using `dmesg | grep brcm` i found the following in the output:
```
brcmfmac 0000:03:00.0: firmware: failed to load brcm/brcmfmac43602-pcie.Apple Inc.-MacBookPro13,3.txt (-2)
brcmfmac 0000:03:00.0: firmware: failed to load brcm/brcmfmac43602-pcie.Apple Inc.-MacBookPro13,3.bin (-2)
```
I then copied the files, making the inside of `/lib/firmware/brcm` look like so:
```
brcmfmac43602-pcie.bin
brcmfmac43602-pcie.txt
brcmfmac43602-pcie.Apple Inc.-MacBookPro13,3.bin
brcmfmac43602-pcie.Apple Inc.-MacBookPro13,3.txt
```
but no dice.
looking harder i then found that the command also reads the following:
```
[    2.981215] usbcore: registered new interface driver brcmfmac
[    3.089788] brcmfmac: brcmf_fw_alloc_request: using brcm/brcmfmac43602-pcie for chip BCM43602/2
[    3.094433] brcmfmac 0000:03:00.0: firmware: direct-loading firmware brcm/brcmfmac43602-pcie.Apple Inc.-MacBookPro13,3.bin
[    3.097480] brcmfmac 0000:03:00.0: firmware: direct-loading firmware brcm/brcmfmac43602-pcie.Apple Inc.-MacBookPro13,3.txt
[    3.102542] brcmfmac 0000:03:00.0: firmware: failed to load brcm/brcmfmac43602-pcie.Apple Inc.-MacBookPro13,3.clm_blob (-2)
[    3.108989] brcmfmac 0000:03:00.0: firmware: failed to load brcm/brcmfmac43602-pcie.Apple Inc.-MacBookPro13,3.clm_blob (-2)
[    3.109010] brcmfmac 0000:03:00.0: firmware: failed to load brcm/brcmfmac43602-pcie.clm_blob (-2)
[    3.109023] brcmfmac 0000:03:00.0: firmware: failed to load brcm/brcmfmac43602-pcie.clm_blob (-2)
[    3.109025] brcmfmac 0000:03:00.0: Direct firmware load for brcm/brcmfmac43602-pcie.clm_blob failed with error -2
[    3.420884] brcmfmac: brcmf_c_process_clm_blob: no clm_blob available (err=-2), device may have limited channels available
[    3.421605] brcmfmac: brcmf_c_preinit_dcmds: Firmware: BCM43602/2 wl0: Nov 10 2015 06:38:10 version 7.35.177.61 (r598657) FWID 01-ea662a8c
[    3.928128] bluetooth hci0: firmware: failed to load brcm/BCM.hcd (-2)
[    3.930127] bluetooth hci0: firmware: failed to load brcm/BCM.hcd (-2)
[    3.934192] Bluetooth: hci0: BCM: 'brcm/BCM.hcd'
```

linux firmware package

things to look into later:
[link 0](https://wiki.debian.org/brcmfmac)
[link 1](https://packages.debian.org/bookworm/firmware-brcm80211)
[link 2](https://www.google.com/search?q=brcmfmac43602-pcie.clm-blob)

#### Solution

a

## things to be lookes at:
- [ ] audio 
- [ ] suspend & hybernate
- [ ] gpu switching
- [ ] bluetooth

## findings
here are some little things i went through in trying to get this thing to work
### Wifi
#### Debian ISO
Debian installer on a usb shows the following when trying to auto-detect wifi card:
```
Some of your hardware needs non-free firmware files to operate. The firmware can be loaded from removable media, such as a USB stick of floppy.
The missing firmware files are:
brcm/brcmfmac43602-pcie.Apple
brcm/brcmfmac43602-pcie.clm_blob
brcm/brcmfmac43602-pcie.txt
```
After not loading firmware files, I am able to view and attempt to join networks.
But attempting to join them will always time out.
#### ArchBootStrap onto USB
The `brcmfmac` driver supports the wifi card in this thing alil bit.
Run `modprobe brcmfmac` and [setup systemd-networkd](https://wiki.archlinux.org/title/systemd-networkd#Wireless_adapter) to allow device to show under `ip addr`.
Attempting to connect to a 2.4G wifi network ising iwctl will fail

## Relevant Links (13,3)
| links |
| :--- |
| https://github.com/Dunedan/mbp-2016-linux#wi-fi |
| https://gist.github.com/cristianmiranda/6f269797b62076c3414c3baa848dda67 |
| https://bugzilla.kernel.org/show_bug.cgi?id=193121 |

## Relevant links (general)
| links |
| :--- |
| https://wiki.debian.org/MacBook/Wireless |
| https://wiki.archlinux.org/title/Mac |
| https://gist.github.com/varqox/42e213b6b2dde2b636ef |
| https://www.debian.org/releases/buster/amd64/apds03.en.html |
| https://askubuntu.com/questions/469209/how-to-resolve-hostnames-in-chroot |
| https://serverfault.com/questions/976289/bash-dpkg-reconfigure-command-not-found |
